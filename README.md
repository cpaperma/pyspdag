# pyspdag: A python package for the ShortestPathDAG data-structure

This package provides a Proof Of Concept implementation of the structure presented in 
the paper **TO ADD LATER**.

The Shortest Path DAG (SpDAG) data-structure is an efficient way to maintain
information of a graph either during an exploration or over a whole rooted graph.
This informations can be used to find rapidly one shortest-path to the root or to 
enumerate for any nodes all shortest-path to the root with constant delay or 
to count the number of shortest-path. 

This package is used to provides experimental evidence of the efficiency of the SpDAG 
as well as a proof of concept of its utilisation for graph exploration and landmark selection. 
For more details refers to the paper cited above.

**WARNING** This package didn't aim to performance and an efficient implementation should 
be done before using it.
