import math
from pyspdag import MultiSpDAG, SpDAG
from pyspdag.tools import PriorityQueue


__all__ = [ "best_path", "all_best_paths", "bestfs_to_BiLayeredDAG", "meet_in_the_middle" ]

def best_path(G, distance, source, target, **kwargs):
    bldag = bestfs_to_BiLayeredDAG(G, distance, source, target, **kwargs)
    return bldag.to_path(source, target)

def all_best_paths(G, distance, source, target, **kwargs):
    bldag = bestfs_to_BiLayeredDAG(G, distance, source, target, **kwargs)
    return bldag.to_enumerate_paths(source, target)

def bestfs_to_BiLayeredDAG(G, distance, source, target,
        depth_factor=.0125, max_size_factor=1, max_size=math.inf,
        bilateral=True, node_filter=lambda v:True,
        verbose=False):
    _LEFT = 0
    _RIGHT = 1
    goals = [ source, target ]
    bldag = MultiSpDAG(goals)
    if bldag.have_joined():
        return bldag
    def priority(v, side):
        d = distance(v, goals[1-side])
        depth = bldag[goals[side]][v].depth()
        return pow(depth, depth_factor)*d
    frontiers = [ set([goals[_LEFT]]), set([goals[_RIGHT]]) ] 
    # main loop
    side = _LEFT
    foundpath_length = None
    while len(bldag) < max_size and frontiers[_LEFT] and frontiers[_RIGHT]:
        # set side to the smallest frontier side if bilateral is True
        if len(bldag[goals[side]]) > len(bldag[goals[1-side]]):
            side = int(bilateral)*(1-side)
        #select next element nxt to treat from the frontier
        nxt = sorted(frontiers[side], key=lambda e:priority(e, side))[0] #should be make efficient with priority queue
        frontiers[side].remove(nxt)
        #treat nxt
        for w in filter(node_filter, G[nxt]):
            # todo: method filtered_adj in DataGraph?
            if w not in bldag[goals[side]]: 
                frontiers[side].add(w)
            bldag.add_connection_to(nxt, w, goals[side])
            if bldag.have_joined() and w in bldag[goals[1-side]]:
                w_bidepth = bldag[goals[side]][w] + bldag[goals[1-side]][w]
                if foundpath_length is None:
                    foundpath_length = w_bidepth
                    if verbose:
                        print("Found path of length {}:\t\t{} ... {} - {} ... {}.".format(
                            foundpath_length, goals[side], nxt, w, goals[1-side]))
                    if max_size == math.inf:
                        max_size = max_size_factor * len(bldag)
                    if max_size_factor == 0:
                        break
                elif w_bidepth < foundpath_length:
                    foundpath_length = w_bidepth
                    if verbose:
                        print("Found shorter path of length {}:\t\t{} ... {} - {} ... {}.".format(
                            foundpath_length, goals[side], nxt, w, goals[1-side]))
                elif w_bidepth == foundpath_length:
                    if verbose > 1:
                        print("  found another path of length {}:\t{} ... {} - {} ... {}.".format(
                            foundpath_length, goals[side], nxt, w, goals[1-side]))
    if verbose:
        print("#visited: {}".format(len(bldag)))
    return bldag
def meet_in_the_middle(G, embedding, distance, roots,
                       depth_factor=.0125, max_size_factor=1, max_size=math.inf,
                       node_filter=lambda v:True,
                       verbose=False):
    """
    Assumed: roots is an nonempty set of vertices of G.
    """
    roots = list(set(roots))
    roots_n = len(roots)
    bary = sum([ embedding[e]/roots_n for e in roots ])
    mldag = MultiSpdDAG(roots)
    if mldag.have_joined():
        return mldag
    def f_priority(root):
        def _priority(v):
            d = distance(v, bary)
            depth = mldag[root][v].depth()
            return pow(depth, depth_factor)*d, d, depth
        return _priority
    frontiers = { r: PriorityQueue(f_priority(r), r) for r in roots }
    # main loop
    while not(mldag.have_joined()) and len(mldag) < max_size and all([frontiers[r] for r in roots]):
        #root = min(filter(lambda r: len(frontiers[r])>0,roots), key=lambda r:len(frontiers[r]))
        root = min(roots, key=lambda r: len(frontiers[r]))
        nxt = frontiers[root].pop()
        for w in filter(node_filter, G[nxt]):
            mldag.add_connection_to(nxt, w, root, priority_queue=frontiers[root])
    if verbose:
        print('Visited size: {}.'.format(len(mldag)))
    return mldag
