import math
from pyspdag.tools import NodesFiltering as nf
from pyspdag import MultiSpDAG, SpDAG

__all__ = [ "shortest_path", "all_shortest_paths", "breadth_join", "check_path",
        "is_connected", "breadth_first_explore", "breadthfs_to_BiSpDAG", "ms_bfs",
        "breadthfs_to_BiSpDAG_iter", "nodes_in_all_shortest_paths"]

def shortest_path(G, source, target, **kwargs):
    kwargs.setdefault('max_size_factor',0)
    bldag = breadthfs_to_BiSpDAG(G, source, target, **kwargs)
    if not bldag.have_joined():
        raise ValueError("No path between {} and {}.".format(source, target))
    return bldag.to_path(source, target)

def all_shortest_paths(G, source, target, **kwargs):
    bldag = breadthfs_to_BiSpDAG(G, source, target, **kwargs)
    if not bldag.have_joined():
        raise ValueError("No path between {} and {}.".format(source, target))
    return bldag.to_enumerate_paths(source, target)

def breadth_join(G, source, target, **kwargs):
    bldag = breadthfs_to_BiSpDAG(G, source, target, **kwargs)
    return bldag.get_join(source, target)

def check_path(G, path, node_filter=nf.all_nodes(), verbose=False, allow_empty=True):
    """
    Returns whether path is a filtered `path` on `G`,
    namely whether two consecutive nodes of `path`
    are connected through an edge of `G` and each
    node of `path` but the extreme ones satisfy the
    `node_filter` function.
    By default, `node_filter` is `nf.all_nodes()`,
    namely accepts any node.
    When `verbose` is set to `True`, additional
    message explaining the answer is given.
    """
    if not path:
        if verbose:
            print("Empty path is a path.")
        return allow_empty
    p = path.copy()
    u = p.pop(0)
    if u not in G:
        if verbose:
            print("Node {} is not a node of {}.".format(u, G))
        return False
    while p:
        v = p.pop(0)
        if v not in G:
            if verbose:
                print("Node {} is not a node of {}.".format(v, G))
            return False
        if v not in G[u]:
            if verbose:
                print("Node {} is not a neighbor of node {} in {}.".format(v, u, G))
            return False
        if not node_filter(v) and p:
            if verbose:
                print("Path-internal node {} is does not satisfy the filtering function.".format(v))
            return False
        u = v
    if verbose:
        print("{} is a path of {}.".format(path, G))
    return True

def is_connected(G, *args, **kwargs):
    """
    If called with 0 (resp. 1 node) argument,
    explore the graph from an arbitrarily
    chosen node (resp. from the node argument),
    and return True if all graph nodes have
    been visited, namely are covered by the
    built SpDAG, or False otherwise.

    If called with a list of nodes (either in
    as first argument or as list of arguments),
    the method explores the graph from these
    nodes and returns True if the exploration
    SpDAGs are pairwise connected, namely
    if all the source nodes given in argument
    are pairwise connected, and False otherwise.

    Optional key-value arguments expected by the
    exploration method `breadth_first_explore`
    and `ms_bfs` such as `node_filter` may be
    provided.
    """
    if len(args) == 0:
        source = next(iter(G))
    elif len(args) == 1:
        # args[0] may be a node or an iterator of nodes
        source = args[0]
    else:
        source = args
    if source in G:
        ldag = breadth_first_explore(G, source, **kwargs)
        return len(ldag) == len(G)
    else:
        mldag = ms_bfs(G, source, **kwargs)
        return mldag.have_joined()

def breadth_first_explore(G, source, node_filter=nf.all_nodes(), max_size=math.inf, max_depth=math.inf):
    ldag = SpDAG(source)
    #
    frontier = [ldag[source]]
    depth = 0
    while frontier and len(ldag) < max_size:
        depth += 1
        frontierNext = list()
        for vt in frontier:
            v = vt[0]
            for w in G._adj[v]:
                wt = ldag.get(w)
                if wt is None:
                    wt = vt.add_connection_to_new_node(w, ldag)
                    frontierNext.append(wt)
                elif w < v and wt[4] == depth + 1:
                    vt.add_sibling(wt)
                elif wt[4] == depth:
                    vt.add_successor(wt)
        frontier = frontierNext
    return ldag

def breadthfs_to_BiSpDAG(G, source, target,
        max_size_factor=1, max_size=math.inf,
        bilateral=True, node_filter=nf.all_nodes(),
        verbose=False):
    """
    Bilateral Breadth First Search
    adapted from networkx.shortest_path
    """
    _LEFT = 0
    _RIGHT = 1
    goals = [source, target]
    bldag = MultiSpDAG(goals)
    if bldag.have_joined():
        return bldag
    frontiers = [ {source}, {target} ]
    side = 0
    while len(bldag) < max_size and frontiers[_LEFT] and frontiers[_RIGHT]:
        if len(bldag[goals[side]]) > len(bldag[goals[1-side]]):
            side = bilateral*(1-side)
        next_frontier = set()
        foundpath_length = None
        for v in frontiers[side]:
            for w in filter(node_filter, G[v]):
                if w not in bldag[goals[side]]:
                    next_frontier.add(w)
                bldag.add_connection_to(v, w, goals[side])
                if w in bldag[goals[1-side]]:
                    w_bidepth = bldag[goals[side]][w] + bldag[goals[1-side]][w]
                    if foundpath_length is None:
                        foundpath_length = w_bidepth
                        if verbose:
                            print("Found path of length {}:\t\t{} ... {} - {} ... {}".format(
                                foundpath_length, goals[side], v, w, goals[1-side]))
                        if max_size == math.inf:
                            max_size = max_size_factor * len(bldag)
                        if max_size_factor == 0:
                            break
                    elif w_bidepth < foundpath_length:
                        foundpath_length = w_bidepth
                        if verbose:
                            print("Found shorter path of length {}:\t\t{} ... {} - {} ... {}.".format(
                                foundpath_length, goals[side], v, w, goals[1-side]))
                    elif w_bidepth == foundpath_length:
                        if verbose > 1:
                            print("  found another path of length {}:\t{} ... {} - {} ... {}.".format(
                                foundpath_length, goals[side], v, w, goals[1-side]))
            if len(bldag) > max_size:
                break
            if foundpath_length is not None and max_size_factor == 0:
                break
        frontiers[side] = next_frontier
    if verbose:
        print("#visited: {}".format(len(bldag)))
    if bilateral:
        return bldag
    else:
        return bldag[goals[side]]

def ms_bfs(G, roots,
        node_filter=nf.all_nodes(),
        halt_condition=None,
        verbose=False):
    #adapted from doi>10.14778/2735496.2735507 (2014)
    mldag = MultiSpDAG(roots)
    if halt_condition is None:
        def _halt_condition(multilayeredDAG=mldag,**kwargs):
            return mldag.have_joined()
        halt_condition = _halt_condition
    frontiers = { r:{r} for r in roots }
    frontiersNext = dict()
    while frontiers and not(halt_condition(multiSpDAG=mldag, frontiers=frontiers)):
        for v, rSet in frontiers.items():
            for n in filter(node_filter, G.adj[v]):
                for root in rSet:
                    if n not in mldag[root]:
                        frontiersNext.setdefault(n, set())
                        frontiersNext[n].add(root)
                    mldag.add_connection_to(v, n, root)
        frontiers = frontiersNext
        frontiersNext = dict()
    return mldag


def breadthfs_to_BiSpDAG_iter(G, source, target,
        node_filter=nf.all_nodes(),
        verbose=False):
    """
    Bilateral Breadth First Search
    adapted from networkx.shortest_path
    """
    _LEFT = 0
    _RIGHT = 1
    goals = [source, target]
    bldag = MultiSpDAG(goals)
    frontiers = [ {source}, {target} ]
    side = 0
    foundpath = False
    while not foundpath:
        if len(bldag[goals[side]]) > len(bldag[goals[1-side]]):
            side = 1-side
        next_frontier = set()
        for v in frontiers[side]:
            for w in filter(node_filter, G[v]):
                if w not in bldag[goals[side]]:
                    next_frontier.add(w)
                bldag.add_connection_to(v, w, goals[side])
                if w in bldag[goals[1-side]]:
                    w_bidepth = bldag[goals[side]][w] + bldag[goals[1-side]][w]
                    foundpath = True
                    yield bldag
        frontiers[side] = next_frontier
    yield bldag


def nodes_in_all_shortest_paths(G, nodes):
	"""
		Compute the convex closure (set of nodes on a shortest path
		between two nodes of nodes)
	"""
	cl = [breadthfs_to_BiSpDAG(G, nodes[i], nodes[j]) for i in range(len(nodes)) for j in range(i+1, len(nodes))]
	cl = [list(d.to_enumerate_paths(*d.roots())) for d in cl]
	return set(sum(sum(cl, []), []))



