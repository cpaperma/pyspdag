import itertools
import functools

_ID = 0
_PRED = 1
_SIBL = 2
_SUCC = 3
_DEPTH = 4

class SpDAGNode(list):
	"""
	A module with most of the Shortest Path DAGs methods
	(see the class `pyspdag.ShortestPathDAG.SpDAG`),
	taking advantages of the richer node structure to
	avoid accesses in dictionary. The methods here take
	node as argument, namely list of size 5 (see the
	`SpDAG` class mentioned above for details) rather
	than node identifiers (which are still accessible as
	head of node list).
	"""
	def __init__(self, vid, pred, spdag=None):
		"""
		Create a new node and connect it to `pred` node.  Hence,
		its depth is set to those of `pred` plus one.  If `pred`
		is `None`, then the new node is seen as the root node and
		its depth is set to `0`.
		"""
		if pred is None: #initialize root node
			super().__init__([ vid, dict(), dict(), dict(), 0 ])
		else:
			super().__init__([ vid, { pred[_ID]: pred }, dict(), dict(), pred[_DEPTH] + 1 ])
			pred[_SUCC][vid] = self
		if spdag is not None:
			spdag[vid] = self

	def __repr__(self):
		s = "<" +\
		str(self[_ID])+":{" +\
		",".join(map(str, self[_PRED].keys())) +"},{" +\
		",".join(map(str, self[_SIBL].keys())) + "},{" +\
		",".join(map(str, self[_SUCC].keys())) + "}:" +\
		str(self[_DEPTH]) +\
		">"
		return s

	# Node attributes and connections
	def vertex_id(self):
		return self[_ID]

	def predecessors(self, node_return=True):
		if node_return:
			yield from self[_PRED].values()
		else:
			yield from self[_PRED].keys()

	def siblings(self, node_return=True):
		if node_return:
			yield from self[_SIBL].values()
		else:
			yield from self[_SIBL].keys()

	def successors(self, node_return=True):
		if node_return:
			yield from self[_SUCC].values()
		else:
			yield from self[_SUCC].keys()

	def neighbors(self, node_return=True):
		if node_return:
			yield from itertools.chain(self[_PRED].values(), self[_SIBL].values(), self[_SUCC].values()) 
		else:
			yield from itertools.chain(self[_PRED].keys(), self[_SIBL].keys(), self[_SUCC].keys()) 

	def depth(self):
		return self[_DEPTH]

	# Tools (path extraction, enumeration, counting (TODO), etc...)
	def get_one_predecessor(self, default=None, node_return=True):
		for k, v in self[_PRED].items():
			if node_return:
				return v
			else:
				return k
		return default

	def generate_one_path_to_root(self, node_return=True):
		ut = self
		while ut is not None:
			if node_return:
				yield ut
			else:
				yield ut[_ID]
			ut = ut.get_one_predecessor()

	def compute_depth(self):
		return functools.reduce(lambda depth, node: depth+1, self.generate_one_path_to_root(node_return=False), 0)

	def to_path(self, reverse=False, node_return=True):
		"""
		Return a path from the shortest path DAG root to vertex
		`self[_ID]`. If `reverse` is True, then the method returns
		the reversed path from node `self[_ID]` to the root.
		"""
		def append_and_return(lst, item):
			lst.append(item)
			return lst
		path = functools.reduce(append_and_return, self.generate_one_path_to_root(node_return=node_return), [])
		if not reverse:
			path.reverse()
		return path

	def to_enumerate_paths(self, reverse=False, node_return=True):
		"""
		Generates all shortest path from the root node of the
		shortest path DAG to node `self`. The yields are list of
		nodes rather than list of vertex ids. When `reverse` is
		`True`, the yielded paths are reversed, namely, they are
		paths from node `self` to the shortest path DAG root.
		"""
		#TODO: networkx avoids popping off lists (stack),
		#   by maintaining the top index instead. Is it better?
		stack = [ (self, self.predecessors()) ]
		while stack:
			u, L = stack[-1]
			try:
				v = next(L)
				stack.append((v, v.predecessors()))
			except StopIteration:
				# no direct access to the shortest path dag.
				#   -> check whether it is the root by looking at its depth.
				if u[_DEPTH] == 0: 
					#format path
					path = list(map(lambda elt: elt[0][_ID], stack))
					if not reverse:
						path.reverse()
					yield path
				stack.pop()

	#dynamic
	def add_predecessor(self, vt):
		self[_PRED][vt[_ID]] = vt
		vt[_SUCC][self[_ID]] = self

	def add_sibling(self, vt):
		self[_SIBL][vt[_ID]] = vt
		vt[_SIBL][self[_ID]] = self

	def add_successor(self, vt):
		self[_SUCC][vt[_ID]] = vt
		vt[_PRED][self[_ID]] = self

	def add_connection_to_new_node(self, v, spdag=None):
		"""
		Creates a new node `vt` with id `v`, with depth one plus depth
		of `self`, connects it as successor of node `self`, indexes it
		in the shortest path DAG `spdag` when given, and returns it.


		Be aware that, we `spdag` is not provided, this operation
		breaks the consistence of the shortest path DAG. Indeed, the
		created and returned node `vt` appears in the successors of
		`self`, but is not indexed in the shortest path DAG main dict.


		This is the only correct way to create new nodes (except when
		creating the root node, in shortest path initialization).
		"""
		vt = SpDAGNode(v, self, spdag)
		return vt

	def add_neighbor(self, vt):
		"""
		Determines the kind of connection to add between existing
		nodes `self` and `vt` according to their depth difference,
		and add it. The larger is the depth difference, the more
		complex is the updating.
		"""
		diff = vt[_DEPTH] - self[_DEPTH]
		if diff < 0:
			return vt.add_neighbor(self)
		elif diff == 0:
			self.add_sibling(vt)
		elif diff == 1:
			self.add_successor(vt)
		elif diff == 2:
			# very local alteration
			#TODO: could be interesting to measure how often this case holds in experimentation.
			#   -> actually, each time the next case holds, this case will hold in recursive calls.
			# depth is decreased by one
			vt[_DEPTH] = self[_DEPTH] + 1 #=vt[_DEPTH] - 1
			# propagation will hold only on successors
			recurs = iter(vt[_SUCC]) #no need to delete vt[_SUCC] now, as it will be overwritten just after
			for wt in vt[_SUCC]:
				del wt[_PRED][vt[_ID]]
			# siblings become successors
			for wt in vt[_SIBL]:
				del wt[_SIBL][vt[_ID]]
				wt[_PRED][vt[_ID]] = vt
			vt[_SUCC] = vt[_SIBL]
			# predecessors become siblings
			for wt in vt[_PRED]:
				del wt[_SUCC][vt[_ID]]
				wt[_SIBL][vt[_ID]] = vt
			vt[_SIBL] = vt[_PRED]
			# self becomes the only predecessor of vt
			vt[_PRED] = { self[_ID]: self }
			for wt in recurs:
				add_connection(vt, wt)
		elif diff == 3:
			# quasi-local alteration
			#TODO: does it have a positive impact on time complexity?
			vt[_DEPTH] = self[_DEPTH] + 1 #=vt[_DEPTH] - 2
			# propagation will hold on successors and siblings
			for wt in vt[_SIBL]:
				del wt[_SIBL][vt[_ID]]
			for wt in vt[_SUCC]:
				del wt[_PRED][vt[_ID]]
			recurs = itertools.chain(vt[_SIBL], vt[_SUCC])
			vt[_SIBL] = dict()
			# predecessors become successors
			for wt in vt[_PRED]:
				del wt[_SUCC][vt[_ID]]
				wt[_PRED][vt[_ID]] = vt
			vt[_SUCC] = vt[_PRED]
			# self becomes the only predecessor of vt
			vt[_PRED] = { self[_ID]: self }
			for wt in recurs:
				add_connection(vt, wt)
		else:
			# propagation will hold on all neighbors
			recurs = itertools.chain(vt[_PRED], vt[_SIBL], vt[_SUCC])
			vt[_DEPTH] = self[_DEPTH] + 1
			for wt in vt[_PRED]:
				del wt[_SUCC][vt[_ID]]
			self[_SUCC][vt[_ID]] = vt
			vt[_PRED] = { self[_ID]: self }
			for wt in vt[_SIBL]:
				del wt[_SIBL][vt[_ID]]
			vt[_SIBL] = dict()
			for wt in vt[_SUCC]:
				del wt[_PRED][vt[_ID]]
				#temporarily, w might have no predecessor
			vt[_SUCC] = dict()
			for wt in recurs:
				add_connection(vt, wt)

