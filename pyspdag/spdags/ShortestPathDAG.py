from pyspdag.spdags.SpDAGNode import SpDAGNode
from itertools import chain
import functools

class SpDAG(dict):
	"""
	A rooted layered Directed Acyclic Graph (DAG).  Intermediate
	object allowing fast exploration, single source shortest paths
	extraction, counting, and enumeration.

	The structure is a dictionary that maps vertex identifiers to
	SpDAGNode's, namely a mutable tuple (i.e., a list) of (fixed)
	length 4, with the following meaning:

	0: the node identifier (e.g., `self[u][0]` equals `u`)
	1: the dictionary of the node predecessors
	2: the dictionary of the node siblings
	3: the dictionary of the node successors
	4: the node depth

	The three dictionaries, namely at indices 1, 2, and 3, are
	sub-dictionaries of the main object dictionary, meaning that
	their keys are keys of the object dictionary, and the
	associated values are consistent, e.g., if `self[u][2][v]` is
	defined, then it is equal (in sense of pointers) to `self[v]`.
	Moreover, the domains of these dictionaries are disjoint and
	form a partition of the neighborhood of the vertex.

	In addition to its main dictionary structure, the SpDAG object
	has two attributes: the root node (`self.rootNode`), and its
	identifier (`self.root`). Thus `self[self.root]` is equal to
	`self.rootNode`. The root node is the unique node to have no
	predecessor, i.e., `self.rootNode[1]` is equal to the empty
	dictionary.


	Most of the SpDAG methods are inherited from the `dict` object
	(e.g., `__contains__`, `__len__`, `__getitem__`), or are
	interfaces to method of the SpDAGNode object, that manipulate
	nodes, rather than vertices. This allows to limit the number
	of dictionary accesses significantly, when maintaining the
	structure under graph alteration, as well as when answering the
	above-mentioned queries.
	"""

	# init
	def __init__(self, root):
		troot = SpDAGNode(root, None)   #root has no predecessor
		self.root = root
		self.rootNode = troot
		super().__init__({root: troot})

	def __repr__(self):
		s = "SpDAG{ "
		s += " ; ".join(map(str, self.values()))
		s += " }"
		return s

	# tools
	def nodes(self, node_return=True):
		if node_return:
			yield from self.values()
		else:
			yield from self.keys()

	def max_depth(self, node_return=False):
		mt = max(self.values(), key=lambda n: n[4])
		if node_return:
			return mt
		else:
			return mt[0]

	def neighbors(self, v, node_return=False):
		return self[v].neighbors(keys=True, node_return=node_return)

	def predecessors(self, v, node_return=False):
		return self[v].predecessors(node_return=node_return)

	def siblings(self, v, node_return=False):
		return self[v].siblings(node_return=node_return)

	def successors(self, v, node_return=False):
		return self[v].successors(node_return=node_return)

	def depth(self, v):
		return self[v].depth()

	def copy(self):
		from copy import deepcopy #standard?
		return deepcopy(self)

	# extract paths
	def to_path(self, v, reverse=False, node_return=False):
		"""
		Returns an arbitrarily chosen shortest path from vertex
		`v` to the root. The return path is a list of node
		identifiers. It is assumed that `v` is a node identifier
		in `self`. A `KeyError` is raised if this is not the case.

		The time complexity is linear in the length of the returned
		path, namely the depth of `v` in `self`.
		"""
		return self[v].to_path(reverse=reverse, node_return=node_return)

	def to_enumerate_paths(self, v, reverse=False, node_return=False):
		"""
		Enumerate all the shortest path from vertex `v` to the root.
		As for `to_path` method, the yielded paths are list of node
		identifiers. It is assumed that `v` is a node identifier in
		`self` (otherwise a `KeyError` is raised) and that the node
		identifiers are totally ordered.

		Time cost between two enumerations is linear in the length of
		the shortest paths.
		"""
		return self[v].to_enumerate_paths(reverse=reverse, node_return=node_return)

	def count_paths(self, v, cache=None):
		"""
		Returns the number of shortest paths from vertex `v` to the
		root. It is assumed that `v` belongs to `self` (otherwise,
		a `KeyError` is raised).
		"""
		if type(v) is int:
			return self.count_paths(self[v])
		if cache is None:
			cache = {self.root:1}
		return sum(map(lambda e:1 + self.count_paths(e, cache=cache), v[1]))

	# to graph & export
	def enumerate_edges(self):
		"""
		Enumerates the SpDAG edges.
		"""
		for u, t in self.items():
			for v in t[2].keys():
				if u < v:
					yield (u, v)
			for v in t[3]:
				yield (u, v)

	def to_tikz(self, path,
			append=False, with_preamble=False,
			scope="tikzpicture", indent=0,
			node_labelling=lambda u: u,
			tikzedges = { 'succ':"->", 'siblings':"--" }):
		if append:
			mode = 'a'
		else:
			mode = 'w'
		with open(path, mode) as tikz:
			if type(with_preamble) is str:
				with open(with_preamble, 'r') as prb:
					for line in prb:
						tikz.write(line)
			elif with_preamble:
				tikz.write('\t'*indent)
				tikz.write("\\documentclass[tikz]{standalone}\n")
				tikz.write('\t'*indent)
				tikz.write("\\usetikzlibrary{graphs}\n")
				tikz.write('\t'*indent)
				tikz.write("\\begin{document}\n")
			tikz.write('\t'*indent)
			tikz.write("\\begin{"+scope+"}%\n")
			indent += 1
			tikz.write('\t'*indent)
			tikz.write('[ x=32mm, y=16mm, baseline ]\n')
			tikz.write('\t'*indent)
			tikz.write("\\graph[simple, no placement] {\n")
			indent += 1
			##
			depth_index = dict()
			some_sibling_edge = False
			for u in self:
				d = self.depth[u]
				depth_index.setdefault(d, [])
				depth_index[d].append(u)
				some_sibling_edge = bool(self.siblings[u])
			def rec_aux_succ(u, first, indent):
				d = self.depth[u]
				l = depth_index[d]
				y = len(l)/2-.5-l.index(u)
				if first:
					tikz.write('\t'*indent)
					tikz.write(' [x={}]\n'.format(d))
				else:
					tikz.write(',\n')
				tikz.write('\t'*indent)
				tikz.write('"{u}"/"{u_lbl}"'.format(u=u, u_lbl=node_labelling(u)))
				tikz.write(' [y={}] '.format(y))
				if self.succ[u]:
					tikz.write(" "+tikzedges['succ']+" ")
					tikz.write('{\n')
					for k, v in enumerate(self.succ[u]):
						rec_aux_succ(v, k==0, indent+1)
					tikz.write('\n')
					tikz.write('\t'*indent)
					tikz.write('}')
			tikz.write('\t'*indent)
			tikz.write('%%% successors\n')
			rec_aux_succ(self.root, True, indent)
			tikz.write(';\n')
			if some_sibling_edge:
				tikz.write('\t'*indent)
				tikz.write('%%% siblings\n')
				##TODO: test siblings edges
				tikz.write('%%%%%% WARNING: tikz export of siblings connection have never been tested!')
				for u in self:
					gsibl = list(filter(lambda v: v>u, self.siblings[u]))
					if gsibl:
						tikz.write('\t'*indent)
						tikz.write('"{u}"/"{u_lbl}"'.format(u=u, u_lbl=node_labelling(u)))
						tikz.write(" "+tikzedges['siblings']+" ")
						tikz.write('{')
						tikz.write(','.join(map(lambda v: '"{v}"/"{v_lbl}"'.format(v=v, v_lbl=node_labelling(v)), gsibl)))
						tikz.write('};\n')
			##
			indent -= 1
			tikz.write('\t'*indent)
			tikz.write("};\n")
			indent -= 1
			tikz.write('\t'*indent)
			tikz.write("\\end{"+scope+"}\n")
			if with_preamble:
				tikz.write('\t'*indent)
				tikz.write("\\end{document}\n")

	def to_networkx(self):
		"""
		Really simple export to `networkx.Graph`.
		"""
		import networkx as nx
		return nx.Graph(incoming_graph_data=self.enumerate_edges())

	# dynamic
	def add_predecessor_connection(self, u, v):
		"""
		assumed: `u` and `v` in `self` with `u` depth equal to `v` depth plus one.
		"""
		ut = self[u]
		vt = self[v]
		ut.add_predecessor(vt)

	def add_sibling_connection(self, u, v):
		"""
		assumed: `u` and `v` in `self`, with same depth.
		"""
		ut = self[u]
		vt = self[v]
		ut.add_sibling(vt)

	def add_successor_connection(self, u, v):
		"""
		assumed: `u` and `v` in `self` with `u` equal to `v` depth minus one.
		"""
		ut = self[u]
		vt = self[v]
		ut.add_successor(vt)

	def add_connection_to_new_node(self, u, v):
		"""
		assumed: `u` in `self` and `v` not in `self`.
		returns the newly created node
		"""
		ut = self[u]
		vt = ut.add_connection_to_new_node(ut, self)
		return vt

	def add_internal_connection(self, u, v):
		"""
		assumed: `u` and `v` are in `self`.
		"""
		ut = self[u]
		vt = self[v]
		self.add_neighbor(ut, vt)

	def add_connection(self, u, v):
		"""
		assumed: `u` is in `self`.
		If `v` does not belong to `self`, then it is created and
		connected to `u`'s node accordingly, and returned.
		"""
		ut = self[u]
		vt = self.get(v)
		if vt is None:
			vt = ut.add_connection_to_new_node(v, spdag=self)
			return vt
		else:
			ut.add_neighbor(vt)

	# remove
	def unsafe_remove_node(self, u, symmetric=True):
		"""
		Simply remove node `u` from the SpDAG `self`, and
		remove symmetric connections, whenever `symmetric` flag
		is set to `True` (default).

		This method is unsafe since it may break the correctness
		of the SpDAG structure. This is indeed broken in
		the following cases:
		(1) the symmetric flag is set to `False`, whence symmetry
		of the SpDAG's connections is not preserved;
		(2) the symmetric flag is set to `True` and a successor
		`v` of `u` has only `u` as predecessor. Hence, after
		removing the `v` to `u` predecessor connection, `v` has
		no predecessor.

		It is up to the user to maintain the structure when
		calling this method. Please consider to call
		<TODO: remove_node>
		`remove_connection` method, if you want a safe node
		removing.
		"""
		if symmetric:
			for v in self.pred[u]:
				self.succ[v].remove(u)
			for v in self.succ[u]:
				self.pred[v].remove(u)
			for v in self.siblings[u]:
				self.siblings[v].remove(u)
		self.depth.pop(u)
		self.pred.pop(u)
		self.succ.pop(u)
		self.siblings.pop(u)

	def connect_to_SpDAG(self, ldag):
		"""
		Arguments:
		+ self: the SpDAG to update
		+ ldag: a disjoint SpDAG to consume and plug in `self`

		The methods connects the SpDAG `ldag` to the SpDAG
		`self` through the nodes which are at the intersection of both
		structures. The connection is made by consuming `ldag` while
		extending `self`. Hence, if the intersection between the two
		SpDAGs is nonempty, then `ldag` is totally consumed by
		the process.  Conversely, whenever the intersection of the two
		SpDAGs is empty, no node is added to `self` nor popped
		off `ldag`.

		Note on complexity:
		Adding a connection `u--v` to `self` is performed in constant
		time as soon as either `v` does not belong to `self` or the
		edge `(u, v)` was already known within `self`. Otherwise, the
		connection may require to propagate depth information and is
		therefore more expensive (todo: upper bound on time complexity).
		"""
		connections = filter(lambda e: e in self, ldag)
		connections = set(connections)
		while connections:
			# pop minimal element `u`
			u = min(connections, key=lambda e: self.depth[e])
			connections.remove(u)
			# store its connections `u_propag` in `ldag`
			u_propag = set(ldag.neighbors(u))
			# pop `u` off from `ldag`
			ldag.unsafe_remove_node(u)
			# connect every `v` from `u_propag` to `u` in `self`
			for v in u_propag:
				self.add_connection(u, v)
				connections.add(v)

	def forward_fix(self, curr_lvl, prv_lvl, d):
		"""
		Arguments:
			+ self: an ill-formatted SpDAG
			+ curr_lvl: a set of nodes of `self`
			+ prv_lvl: a set of nodes of `self`
			+ d: a positive integer

		Assumed:
			+ `curr_lvl` and `prv_lvl` are disjoint sets.
			+ each node of `curr_lvl` admits a predecessor in
				`prv_lvl`.
			+ the nodes from `prv_lvl` have all depth `d-1` and
				are at distance `d-1` of the root of `self`.
			+ the nodes from `curr_lvl` are at distance `d` of
				the root of `self`. Their current, possibly
				incorrect, depth in `self` is either `d-1` or
				`d+1`.
			+ only connections between a node from `prv_lvl` and
				a node from its complement are invalid with
				respect to the node depths in `self`. Moreover,
				these connections are necessarily pred/succ
				connections, where the node in `prv_lvl` is the
				predecessor of the other one.
				This allows having some node depth incorrect,
				namely, not representing the real distance of
				the node to the root of `self` in `self`.
			+ all the nodes at distance less than `d` of the
				root of `self` have correct depth in `self` and
				correct outgoing pointers. (A subset of them is
				the set `prv_lvl`.)
			+ each node at distance `d+1` of `self` root either
				belongs to `curr_lvl` or have correct depth in
				`self`, i.e., depth `d+1`.
		There is no systematic verification of these conditions,
		except for the depth of nodes from `curr_lvl` with
		respect to `d`.

		Calls:
			The method is called in `suck_me_from` method
			(itself called by `remove_connection`) and in
			`rotate` method.
		"""
		if not curr_lvl:
			return
		curr_lvl_nxt = set()
		prv_lvl_nxt = set()
		while curr_lvl:
			v = curr_lvl.pop()
			v_depth = self.depth[v]
			v_pred = self.pred[v]
			propagate = self.succ[v].copy()
			strong_v_pred = v_pred.difference(prv_lvl, curr_lvl)
			if v_depth == d-1:
				if strong_v_pred:
					# depth `v` is kept unchanged
					#   => predecessors in `prv_lvl` become siblings
					for u in v_pred.intersection(prv_lvl):
						self.succ[u].remove(v)
						v_pred.remove(u)
						self.siblings[u].add(v)
						self.siblings[v].add(u)
				else:
					# depth of `v` is set to `d` (i.e., incremented)
					#   => siblings become predecessors
					#   => propagate to (original) successors, if any
					self.depth[v] = d
					for w in self.siblings[v]:
						self.siblings[w].remove(v)
						self.succ[w].add(v)
						v_pred.add(w)
					self.siblings[v] = set()
					if propagate:
						curr_lvl_nxt.update(propagate)
						prv_lvl_nxt.add(v)
			elif v_depth == d+1:
				# depth of `v` is set to `d` (i.e., decremented)
				#   => siblings become successors and predecessors
				#	   not belonging to `prv_lvl` become siblings,
				#	   i.e., in `strong_v_pred`
				#   => propagate to original successors, if any
				self.depth[v] = d
				for w in self.siblings[v]:
					self.siblings[w].remove(v)
					self.pred[w].add(v)
					self.succ[v].add(w)
				self.siblings[v] = set()
				for u in strong_v_pred:
					self.succ[u].remove(v)
					v_pred.remove(u)
					self.siblings[u].add(v)
					self.siblings[v].add(u)
				if propagate:
					curr_lvl_nxt.update(propagate)
					prv_lvl_nxt.add(v)
			else:
				raise ValueError("Nodes in first set argument (`curr_lvl`) should have depth `d-1` or `d+1`.",
						"Here, node '{v}' has depth {v_depth} while `d` equals {d}.".format(v=v, v_depth=v_depth, d=d))
		self.forward_fix(curr_lvl_nxt, prv_lvl_nxt, d+1)

	def suck_me_from(self, v):
		"""
		Arguments:
			+ self: the SpDAG to be sucked, possibly ill-formed by
				allowing `v` (only) to have no predecessors.
			+ v: the root of the sucking SpDAG
		Return:
			+ a SpDAG rooted in `v`, which contains nodes popped off
				from `self`, plus, as leaves, some connection nodes in
				`self`.
		Process:
			+ a SpDAG `ldag` rooted in `v` is created.
			+ if `v` has no predecessors in `self`, then `v` is popped off
				`self`, and its successors are added to `ldag`. In `self`,
				these successors may have no predecessors, since `v` has
				been popped off. Thus the above action is repeated for each
				successor and so on.

		Assumed when entering the loop:
			+ the nodes from `prv_lvl` belongs to `ldag` but not to `self`;
			+ the nodes from `curr_lvl` belongs to both `self` and `ldag`;
			+ the sets of predecessors of `w` in `self` and `ldag` form a
				partition of `w`'s predecessors in the original SpDAG,
				such that predecessors of `w` in `ldag` are not any longer
				belonging to the `self` and belongs to `prv_lvl`, while the
				set of predecessors of `w` in `self` might be empty (and
				should be updated in this case) and is disjoint from the
				set of nodes covered by `ldag`;

		Loop process:
			Each node `w` from `curr_lvl` is examined:

			First, the siblings of `w` in `ldag` are restricted to those
			belonging to `curr_lvl` (this operation globally preserves
			the symmetry of the siblings relation);

			Then the sets of predecessors and siblings of `w` in `self` are
			considered:
			+ if `w` admits a predecessor in `self`, then it becomes a leaf
				in `ldag` by setting its set of successors to empty set,
				and it is kept unchanged in `self`.
			+ otherwise, if `w` admits a sibling in `self` not belonging to
				`curr_lvl`, then it also becomes a leaf of `ldag`, again by
				setting its successor set to empty set. However, contrary to
				the previous case, its depth is checked in `self` and
				forwardly propagated by the method `forward_fix`,
				since its siblings should become its new predecessors whence
				its depth is incremented by 1.
			+ otherwise, `w` has no non-forward connection with `self`, and
				it is popped off from `self` while its successors are added
				to `ldag` and `nxt_lvl`, and the set of `w`'s successors is
				set accordingly. (Popping off `w` from `self` requires to
				eliminate symmetric relations such as siblings and
				successors/predecessors.)

			Finally, the set `curr_lvl` becomes the new `prv_lvl` set, while
			`nxt_lvl` becomes `curr_lvl`. The loop is exited when `curr_lvl`
			is empty.
		"""
		ldag = SpDAG(v)
		prv_lvl = set()
		curr_lvl = {v}
		while curr_lvl:
			nxt_lvl = set()
			for w in curr_lvl:
				w_sibl = self.siblings[w]
				self_w_sibl = w_sibl.difference(curr_lvl)
				ldag_w_sibl = w_sibl.intersection(curr_lvl)
				ldag.siblings[w] = ldag_w_sibl
				#
				self_w_pred = self.pred[w]
				if self_w_pred:
					# found a predecessor connection of `w` to `self` through predecessors
					#   => make `w` a leaf of `ldag`
					ldag.succ[w] = set()
				elif self_w_sibl:
					# found a predecessor connection of `w` to `self` through siblings
					#   => make `w` a leaf of `ldag`
					#   => increment `w`'s depth in `self` and propagate forwardly.
					ldag.succ[w] = set()
					for x in self_w_sibl:
						self.siblings[x].remove(w)
						self.siblings[w].remove(x)
						self.succ[x].add(w)
						self.pred[w].add(x)
					self.forward_fix({w}, self_w_sibl, self.depth[w]+1)
				else:
					# no predecessor connection of `w` to `self` has been found.
					#   => add `w`'s successors to `ldag` and to `nxt_lvl` set
					#   => pop `w` off from `self`
					ldag_w_depth = ldag.depth[w]
					w_succ = self.succ[w]
					ldag.succ[w] = w_succ
					for x in w_succ:
						ldag.depth.setdefault(x, ldag_w_depth+1)
						ldag.pred.setdefault(x, set())
						ldag.pred[x].add(w)
						nxt_lvl.add(x)
						self.pred[x].remove(w)  #from here: pop `w` off from `self`
					for x in w_sibl:
						self.siblings[x].pop(w)
					self.pred.pop(w)
					self.siblings.pop(w)
					self.succ.pop(w)
					self.depth.pop(w)
			prv_lvl = curr_lvl
			curr_lvl = nxt_lvl
		return ldag

	def remove_connection(self, u, v):
		if u not in self.depth or v not in self.depth or u not in self.neighbors(v):
			raise ValueError('Node arguments should form a connection of the SpDAG.')
		diff = self.depth[v] - self.depth[u]
		if diff < 0:
			self.remove_connection(v, u)
		elif diff == 0:
			self.siblings[u].remove(v)
			self.siblings[v].remove(u)

		elif diff == 1:
			self.pred[v].remove(u)
			self.succ[u].remove(v)
			ldag = self.suck_me_from(v)
			self.connect_to_SpDAG(ldag)

	# re-root
	def rotate(self, new_root):
		"""
		Arguments:
		+ self: the SpDAG
		+ new_root: a successor of `self` root or the root itself
		"""
		old_root = self.root
		if old_root == new_root:
			return
		if self.depth[new_root] != 1:
			raise ValueError("Node argument '{}' should be a successor of the SpDAG root '{}'.".format(new_root, old_root))
		propagate = self.succ[new_root].copy()
		propagate.add(old_root)
		# set new_root as root
		self.root = new_root
		self.depth[new_root] = 0
		# predecessor (only `old_root`) and siblings of `new_root` become successors
		self.succ[old_root].remove(new_root)
		self.pred[new_root].remove(old_root)
		self.pred[old_root].add(new_root)
		self.succ[new_root].add(old_root)
		for w in self.siblings[new_root].copy():
			self.siblings[w].remove(new_root)
			self.siblings[new_root].remove(w)
			self.pred[w].add(new_root)
			self.succ[new_root].add(w)
		# propagate depth on original successors of `new_root` as well as `old_root`
		self.forward_fix(propagate, {new_root}, 1)

	def root_me_in(self, new_root):
		path_from_root = self.to_path(new_root)
		for w in path_from_root:
			self.rotate(w)

	def remove_node(self, node):
		for v in self.succ[node]:
			self.remove_connection(node, v)
		for v in self.prec[node]:
			self.remove_connection(v, node)
		for v in self.siblings[node]:
			self.remove_connection(node, v)

		self.prec.pop(node)
		self.succ.pop(node)
		self.depth.pop(node)
		self.siblings.pop(node)

class MultiSpDAG:
	"""
	self.SpDAGs	each root is mapped to the its SpDAG
	self.nodes:		 each node is mapped to the set of SpDAGs (given by their roots) to which it belongs
	self.join		   each unordered pair of distinct roots is mapped to a set of elements
							in the intersection of their associated SpDAG
							which minimize the sum of their respective depth
							(i.e., are on the shortest found between the two roots)
							and, among these nodes, minimize the absolute value of their depth difference
							(i.e., are on the middle of these shortest paths).
	"""
	def __init__(self, roots):
		"""
		Assumed: roots is a set of hashable and comparable ids.
		Initialize the structure.
		If roots is an iterator, repetitions of elements are ignored.
		"""
		roots = set(roots)
		self.SpDAGs = { r: SpDAG(r) for r in roots }
		self.nodes = { r: {r} for r in roots }
		self.join = { (r1, r2): set() for r1 in roots for r2 in roots if r1 < r2 }

	def __getitem__(self, r):
		"""
		Returns the SpDAG associated to the root `r`.
		Raises KeyError exception if `r` is not a SpDAG root.
		"""
		return self.SpDAGs[r]

	def __iter__(self):
		"""
		Returns an iterator on self.SpDAGs.
		"""
		return iter(self.SpDAGs)

	def __len__(self):
		"""
		Returns the total number of nodes covered by the SpDAGs.
		"""
		return len(self.nodes)

	def __set__(self):
		"""
		Returns the set of nodes covered by at least one of the SpDAGs.
		"""
		return set(self.nodes)

	def __contains__(self, v):
		"""
		Returns True if v is covered by at least one of the SpDAGs, and False otherwise.
		"""
		return v in self.nodes

	def __repr__(self):
		s = "MULTISPDAG{\n\t"
		s += ",\n\t".join(map(lambda r: str(r)+": "+str(self.SpDAGs[r]), self.SpDAGs))
		s += "\n\t}"
		return s

	def roots(self):
		return set(self.SpDAGs.keys())

	# join
	def get_join(self, *args):
		"""
		Returns the union of join set.
		Optional arguments may restrict roots
		or pair of roots to be considered.
		"""
		if len(args) == 0:
			roots = list(self.roots())
		else:
			roots = list(args)
		if type(roots[0]) is tuple:
			proots_l = list(roots)
		else:
			proots_l = []
			for r1 in roots:
				for r2 in filter(lambda r: r>r1, roots):
					proots_l.append((r1, r2))
		join = set()
		for proots in proots_l:
			join.update(self.join[proots])
		return join

	def have_joined(self, roots=None):
		"""
		Return `True` or `False` according to whether
		all the SpDAGs specified by `roots` are pairwise intersected.
		By default, roots is the set of all roots of the MultiSpdDAG.
		"""
		if roots is None:
			roots = set(self)
		sub_join = filter(lambda k: k[0] in roots and k[1] in roots, self.join)
		sub_join = map(lambda k: self.join[k], sub_join)
		return all(sub_join)

	def update_join(self, v, root):
		"""
		Assumed: v in SpDAGs[root] is a SpDAGNode.
		Update the join elements associated to any unordered pair of distinct roots (r,root)
		where r is in self.nodes[v], according to the depth of v in the respective SpDAGs.
		This method is called after having added a connection to v in SpDAGs[root].
		"""
		dv_root = self.SpDAGs[root][v].depth()
		for r in self.nodes[v]:
			if r == root:
				continue
			proots = tuple(sorted((r, root)))
			self.join[proots].discard(v)
			if self.join[proots]:
				t = next(iter(self.join[proots]))
				dt_root = self.SpDAGs[root][t].depth()
				dt_r = self.SpDAGs[r][t].depth()
				dv_r = self.SpDAGs[r][v].depth()
				diff = (dt_root + dt_r) - (dv_root + dv_r)
				balanced = abs(dt_root - dt_r) - abs(dv_root - dv_r)
				if (diff, balanced) > (0, 0):
					self.join[proots] = {v}
				elif (diff, balanced) == (0, 0):
					self.join[proots].add(v)
			else:
				self.join[proots].add(v)

	# add
	def add_connection_to(self, u, v, root, priority_queue=None):
		"""
		The connection `u`-to-`v` is asymmetrically considered in the SpDAG corresponding to root.
		An exception is raised whenever `u` does not belong to the SpDAG.
		When a connection is made, `priority_queue` is updated accordingly if given.
		"""
		ld = self.SpDAGs[root]
		self.nodes.setdefault(v, set())
		self.nodes[v].add(root)
		ld.add_connection(u, v)
		self.update_join(v, root)
	# tools
	def closest_roots(self, v):
		import math
		min_depth = math.inf
		closest_roots = []
		for r in self.nodes[v]:
			ld = self.SpDAGs[r]
			d = ld.depth[v]
			if d < min_depth:
				min_depth = d
				closest_roots = [r]
			elif d == min_depth:
				closest_roots.append(r)
		return closest_roots

	def closest_root(self, v):
		"""
		Returns a closest root among the roots of
		the SpDAGs covering `v`.
		"""
		root = min(self.nodes[v],
				key=lambda r: self.SpDAGs[r].depth[v])
		return root

	def to_path(self, v, *args):
		"""
		Assumed: the second argument is a
		MultiSpDAG root.

		When called with only one argument `v`,
		which is not a root, the method returns
		a path from `v` to a closest root
		chosen among the roots of the
		SpDAGs including `v` (namely, in
		`nodes[v]`) according to the class
		method `closest_root`.
		This root choice can be avoided by
		providing as second argument a root of
		a SpDAG including `v`.

		If, otherwise, the first argument is a
		root, then the method returns a path
		from that root to another which is
		either arbitrarily chosen among the
		distinct roots of the MultiSpDAG,
		or is provided as second argument.

		In any case, the second argument, if
		provided, is assumed to be one of the
		MultiSpDAG roots.
		"""
		if len(args) > 1:
			raise ValueError("Method expects one or two arguments; not more.")
		elif len(args) == 1:
			w = args[0]
		else:
			w = None
		roots = self.roots()
		if v not in roots:
			if w is None:
				w = self.closest_root(v)
			ld = self.SpDAGs[w]
			p = ld.to_path(v)
		else:
			if w is None:
				w = next(filter(lambda r: r != v, roots))
			ldv = self.SpDAGs[v]
			ldw = self.SpDAGs[w]
			proots = tuple(sorted((v, w)))
			v = next(iter(self.join[proots]))
			pv = ldv.to_path(v, reverse=False)
			pw = ldw.to_path(v, reverse=True)
			pv.pop()
			p = pv+pw
		return p

	def to_enumerate_paths(self, root1, root2):
		"""
		Return an iterators of all paths from root1
		to root2.
		The computation time between enumeration of
		two paths if linear in the length of the paths.
		"""
		ld1 = self.SpDAGs[root1]
		ld2 = self.SpDAGs[root2]
		proots = tuple(sorted((root1, root2)))
		for v in self.join[proots]:
			p1_it = ld1.to_enumerate_paths(v, reverse=False)
			p1_it = list(p1_it)
			p2_it = ld2.to_enumerate_paths(v, reverse=True)
			for p2 in p2_it:
				for p1 in p1_it:
					yield p1[:-1]+p2

	def to_enumerate_root_paths(self, *args):
		"""
		Enumerate paths from root to root.
		Argument are optional and may be iterable or single values.
		Arguments are pre-treated in such a way that a root-to-root path or its
		reverse occurs exactly once.

		If two arguments are given, enumerate paths from the roots from the
		first one, to the roots from the second one.
		E.g., `to_enumerate_paths(r1, r2)` will enumerate paths from `r1` to `r2`,
		while `to_enumerate_paths([r1a, r1b, r1c], [r2a, r2b])` will enumerate
		paths from any root `r1*` to any distinct root `r2*`.
		If a root occur in the two iterator arguments, only paths from the least
		one to the largest one are enumerated.

		If only one argument is provided, the method enumerates paths from the
		roots from this argument to any distinct root.

		Finally, if no argument are given, the method enumerates paths from any
		root to any larger root.
		"""
		if len(args) > 3:
			raise ValueError("At most 2 arguments expected.")
		elif len(args) == 2:
			root1_l = args[:0]
			root2_l = args[1:]
			proots_l = [
					tuple(sorted((r1, r2)))
					for r1 in root1_l for r2 in root2_l
					if r1 != r2 and (r1 < r2 or (r2 not in root1_l and r1 not in root2_l))
					]
		elif len(args) == 1:
			root1_l = list(args)
			proots_l = [ tuple(sorted((r1, r2))) for r1 in root1_l for r2 in self.roots() if r1 != r2 ]
		else:
			roots = self.roots()
			proots_l = [ (r1, r2) for r1 in roots for r2 in roots if r1 < r2 ]
		for (root1, root2) in proots_l:
			for p in self.to_enumerate_paths(root1, root2):
				yield p

	def count_paths(self, root1, root2):
		ld1 = self.SpDAGs[root1]
		ld2 = self.SpDAGs[root2]
		proots = tuple(sorted((root1, root2)))
		c = 0
		for v in self.join[proots]:
			c1 = ld1.count_paths(v)
			c2 = ld2.count_paths(v)
			c += c1*c2
		return c

	# to graph
	def enumerate_edges(self):
		for ldag in self.SpDAGs.values():
			for u, v in ldag.enumerate_edges():
				yield u, v

	def remove_node(self, n):
		roots = self.nodes[n]
		for r in roots:
			self[r].remove_node(n)
		self.nodes.pop(n)

	def trim(self):
		"""
		Remove all nodes that are not in a shortest path between
		two roots.
		Complexity is in O(n) where n is the number of nodes.
		"""
		frontier = set()
		for join in self.join.values():
			frontier.update(join)
		seen = frontier.copy()
		while frontier:
			a = frontier.pop()
			roots = self.nodes[a]
			for r in roots:
				k = self[r].pred[a].difference(seen)
				frontier.update(k)
				seen.update(k)

	def get_trimed_dag(self, pair_of_roots=None):
		"""
		Return a directed acyclic graph from pair_of_roots
		of shortest path. If pair_of_roots is None, it
		take the two first roots.
		"""
		if pair_of_roots is None:
			pair_of_roots = tuple(self.roots())[:2]
		source, target = pair_of_roots
		join = self.get_join(source, target)
		import networkx as nx
		DAG = nx.DiGraph()
		ldagS = self.SpDAGs[source]
		ldagT = self.SpDAGs[target]
		reverse = False
		for ldag in [ldagS, ldagT]:
			frontier = set(join)
			while frontier:
				next_frontier = set()
				for n in frontier:
					if not reverse:
						DAG.add_edges_from([(n, e) for e in ldag[n][1]])
					else:
						DAG.add_edges_from([(e, n) for e in ldag[n][1]])
					next_frontier.update(ldag[n][1])
				frontier = next_frontier
			reverse = True
		return DAG
