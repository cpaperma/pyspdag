from gensim.models import Word2Vec
from random import choice
import os
from tempfile import TemporaryFile
class WalkIter:
    def __init__(self, filepath=None, verbose=False):
        if filepath is None:
            f = TemporaryFile(mode="r+")
        else:
            f = open(filepath, "r+")
        self.file = f
        self.verbose=verbose
        self.linecount = 0
    def generator(self):
        self.file.seek(0)
        i = 0
        for l in self.file:
            i += 1
            if self.verbose:
                print("reading:", round(100*i/self.linecount), "%",end="\r")
            yield l.strip().split("\t")

    def __iter__(self):
        return self.generator()

    def append(self, walk):
        self.file.write("\t".join(walk)+"\n")
        self.linecount += 1

def default_walks(graph, sample, length, verbose=False):
    """
        Return an on-file iterator over walks so it scale
        on big graph.

        Input:
            Sample: the number of walks will be sample*nb of nodes.
            Length: length of the walks
    """
    nodes = list(graph.nodes())
    walks = WalkIter(verbose=verbose)
    n = len(nodes)
    for i in range(sample*n):
        if verbose:
            print(round(100*i/(sample*n), 2), "%", end="\r")
        root = choice(nodes)
        walk = []
        for _ in range(length+1):
            walk.append(str(root))
            root = choice(list(graph[root]))
        walks.append(walk)
    return walks

def WalkToVec(graph, walks=default_walks, sample=10, length=80, dim=60, verbose=False, **kwargs):
    """
        produce an embedding by performing a random walk
        on the graph and feeding Word2Vec to it.
        kwargs is transmitted to Word2Vec
        Custom walks can be transmitted.
    """
    defaults = {
            "size":dim,
            "window":10,
            "min_count":1
    }
    defaults.update(kwargs)
    if verbose: print("Compute the walks")
    _walks = walks(graph, sample, length, verbose=verbose)
    w2v = Word2Vec(_walks, **defaults)
    return {n:w2v[str(n)] for n in graph.nodes()}
