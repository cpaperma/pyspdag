from scipy.sparse.linalg import svds
def embed_svd_adjency(graph, dim=45):
    nodes = list(graph.nodes())
    Adj = nx.adjacency_matrix(graph).astype("f")
    U, Sigma, V = svds(Adj, k=k)
    U = V.transpose()
    vect = {nodes[i]:U[i] for i in range(len(nodes))}
    return vect


