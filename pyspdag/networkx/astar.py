from heapq import heappush as push, heappop as pop
from itertools import count

import networkx as nx
from networkx.utils import not_implemented_for

__all__ = ['astar_path', 'astar_path_length']


@not_implemented_for('multigraph')
def astar_path(G, source, target, heuristic=None, weight='weight', **kwargs):
    if source not in G or target not in G:
        msg = 'Either source {} or target {} is not in G'
        raise nx.NodeNotFound(msg.format(source, target))
    if heuristic is None:
        def heuristic(u, v):
            return 0

    explored_size = 1
    c = count()
    queue = [(0, next(c), source, 0, None)]
    enqueued = {}
    explored = {}

    while queue:
        _, __, curnode, dist, parent = pop(queue)

        if curnode == target:
            path = [curnode]
            node = parent
            while node is not None:
                path.append(node)
                node = explored[node]
            path.reverse()
            return path, explored_size

        if curnode in explored:
            continue
        explored[curnode] = parent

        for neighbor, w in G[curnode].items():
            if neighbor in explored:
                continue
            explored_size += 1            
            ncost = dist + w.get(weight, 1)
            if neighbor in enqueued:
                qcost, h = enqueued[neighbor]
                if qcost <= ncost:
                    continue
            else:
                h = heuristic(neighbor, target)
            enqueued[neighbor] = ncost, h
            push(queue, (ncost + h, next(c), neighbor, ncost, curnode))

    raise nx.NetworkXNoPath("Node %s not reachable from %s" % (source, target))

@not_implemented_for('multigraph')
def _bilateral_astar_path(G, source, target, heuristic=None, weight='weight', **kwargs):
    if source not in G or target not in G:
        msg = 'Either source {} or target {} is not in G'
        raise nx.NodeNotFound(msg.format(source, target))
    if heuristic is None:
        def heuristic(u, v):
            return 0

    explored_size = 2
    c = count()

    queue = [ (0, next(c), source, 0, None) ]

