import heapq

class PriorityQueue(dict):
    """
    A class that implements heap queue algorithms, where item priorities are
    computed through a `priority_function` given at object initialization.
    The function should return an ordered-typed value accepted by the `heapq`
    module, that is computed from the unique argument, namely the item. Yet,
    it may use external objects whence the priority of a given item might
    change. The `PriorityQueue` object does not detect such changes and it is
    up to the programmer to call the method `update` passing with the item as
    argument. Alternatively, the programmer may rebuild the whole heap queue
    by recomputing the item priorities, using the method `rebuild`.

    
    The class includes the following methods:
    + `pop`: pops the item with least priority off the queue and returns it.
    + `push`: pushes an item to the queue. The item may or may not belong to
        the queue at the beginning. A priority can be fixed, instead of being
        computed using the `priority_function`, by providing it through the
        keyword argument `priority`. This will set the priority of the item
        manually. This priority will be protected against later rebuild,
        except if the `updatable` keyword argument is provided as well and
        has value `False`. Notice that the item priority is still updatable
        by manually pushing it (with new priority, or without for computing
        it using the object function `priority_function`). The updating
        behavior may be controlled by the keyword argument `force` (default
        is `True`) which allows or forbid updating non-updatable items.
    + `is_empty`: returns `True` if the queue is empty and `False` otherwise.
    + `rebuild`: rebuilds the heap queue by recomputing the priority of each
        items which is not marked as non-updatable. This exception is ignored
        if the keyword argument force is set to `True` (default is `False`).
        Though the method can be called by the programmer, it is intended to
        be called internally, after some changes, to save memory space. The
        object attribute `rebuild_factor` allows to control how often the
        method is called.
    + `clean_queue`: clean the queue. This routine is automatically called
        after some operations on the queue to free space. To improve
        performance the `clean_queue_factor` allows to control whether the
        operation is performed or not (set it to 0 or 1 to free maximal
        space, or to 2 or more to prevent doing to many cleaning operations).
    + `smallest`: returns the item with least priority.
    + `update`: does exactly the same as the `push` method (with the same
        optional keyworded arguments), except that the item to update is
        required to belong to the queue at the beginning. A KeyError is
        raised if this is not the case.
    + `remove`: remove an item from the queue. A KeyError is raised if the
        item does not belong to the queue.
    + `discard`: same a remove but do not raise error if the item does not
        belong to the queue.
    """
    def __init__(self, priority_function, arg=[], clean_queue_factor=2, incremental_rebuild=False):
        self.clean_queue_factor = clean_queue_factor
        self.incremental_rebuild = incremental_rebuild
        self.priority_function = priority_function
        try:
            #create queue dictionary from iterable arg
            super().__init__({ k: None for k in arg })
        except TypeError:
            #or create dictionnary with arg as unique key if not iterable
            super().__init__({ arg: None })#values is just a key
        # priorities will be computed after
        self.rebuild(incremental_rebuild=incremental_rebuild, force=True)

    def is_empty(self):
        return bool(self)

    def push(self, v, priority=None, updatable=None, force=True):
        if priority is None:
            priority = self.priority_function(v)
        if updatable is None:
            updatable = priority is None
        newt = [ priority , v, updatable, True ]
        oldt = self.get(v, None)
        if oldt is not None:
            if oldt[0] == newt[0]: # priority have not changed.
                return
            if (not force) and (not oldt[-2]): # item has non-updatable priority
                return
            oldt[-1] = False
        heapq.heappush(self.queue, newt)
        super().__setitem__(v, newt)
        self.clean_queue(clean_queue_factor=self.clean_queue_factor)

    def pop(self):
        _, v, _, enabled = heapq.heappop(self.queue)
        while not enabled:
            _, v, _, enabled = heapq.heappop(self.queue)
        del self[v]
        return v
    
    def smallest(self):
        _, v, updatable, enabled = self.queue[0]
        while not enabled:
            heapq.heappop(self.queue)
            _, v, updatable, enabled = self.queue[0]
        return v
    
    def update(self, v, priority=None, updatable=None, force=True):
        if v not in self:
            raise ValueError('Element {} not in PriorityQueue.'.format(v))
        self.push(v, priority=priority, updatable=updatable, force=force)

    def remove(self, v):
        self[v][-1] = False
        super().__delitem__(v)
        self.clean_queue(clean_queue_factor=self.clean_queue_factor)

    def discard(self, v):
        if v in self:
            self.remove(v)

    def rebuild(self, incremental_rebuild=None, force=False):
        if incremental_rebuild is None:
            incremental_rebuild = self.incremental_rebuild
        self.queue = []
        for v, t in self.items():
            if (t is not None) and (not t[-1]):
                del self[v]
            if force or (t is None) or t[-2]:
                t = [ self.priority_function(v), v, True, True ]
                super().__setitem__(v, t)
            if incremental_rebuild:
                heapq.heappush(self.queue, (v, t))
        if not incremental_rebuild:
            self.queue = list(self.values())
            heapq.heapify(self.queue)

    def clean_queue(self, clean_queue_factor=1, incremental_rebuild=None):
        if len(self)*clean_queue_factor > len(self.queue):
            return
        if incremental_rebuild is None:
            incremental_rebuild = self.incremental_rebuild
        if incremental_rebuild:
            self.queue = []
            for v, t in self.items():
                heapq.heappush(self.queue, (v, t))
        else:
            self.queue = list(self.values())
            heapq.heapify(self.queue)

    def priority(self, v):
        return self[v][0]

    def __delitem__(self, v):
        self.remove(v)

    def __setitem__(self, v, t):
        self.push(v)

