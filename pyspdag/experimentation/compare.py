from statistics import mean, stdev
import matplotlib.pyplot as plot
import random
import numpy

__all__ = [ "range_sp_comp", "sample_sp_comp", "SPMethod", "format_sp_results_comp" ]


class SPMethod:
    """
    An object describing a shortest path algorithm. Attributes, that
    are settable through arguments of the initialization method, are:
    + `sp_algo_generator`: a function generating a simple shortest
        path function. It takes three arguments `g`, `h`, and `x`,
        where `g` is a graph, `h` is (possibly) a heuristic function,
        and `x` is a variable parameter for experimentation. This
        function should return a simple shortest path function (see
        below).
    + `hfunc_generator`: a function generating a heuristic function
        (or not). It takes two arguments, namely a graph `g` and the
        variable parameter `x`. It should return a heuristic function
        as described below.
    + `color`: a color (`str`), to plot results. This attribute is
        optional; default is `'black'`.
    + `description`: a string (`str`) describing the shortest path
        method. This is optional; default is `'<no description>'`.

    Simple shortest path function:
        A function taking only two arguments (namely the source and the
        target vertices of the searched path) and returning a pair
        `(path, expl_size)` where `path` is the found path (`list` of
        vertices) and `expl_size` is an integer (`int`) indicating the
        size of the portion of `g` that has been explored during the
        search. Notice that the function does not take the underlying
        graph as argument which should therefore be understood globally
        in the function (see `sp_algo_generator` above).

    Heuristic function:
        Could be `None`, if no heuristic are expected. Otherwise, it
        should be a function mapping pair of vertices to comparable
        values, as expected by the `networkx` implementation of A*
        algorithm. Just as for simple shortest path functions, the
        function itself does not take the underlying graph as argument
        (see `hfunc_generator`) above.
    """
    def __init__(self, sp_algo_generator, hfunc_generator=None, color='black', description="<no description>"):
        self.sp_algo_gen = sp_algo_generator
        self.hfunc_gen = hfunc_generator
        self.color = color
        self.descr = description

def range_sp_comp(x_range, graph_generator, to_compare, reference, constant=False, sample_generator=64, verbose=False, in_place=False):
    """
    + `x_range`:            range of the variable parameter `x`
    + `graph_generator`:    function generating a graph from the value of `x`.
    + `to_compare`:         list of `SPMethod` objects specifying the shortest
                                path algorithms to compare.
    + `reference`:          a `SPMethod` object specifying the reference shortest
                                path algorithm.
    + `constant`:           a `bool` indicating whether the graph, the sample, and
                                the reference algorithm should be recomputed for
                                each value of the variable parameter `x`. Set it
                                to `False` if the generated graph, the generated
                                sample, and the reference shortest path algorithm
                                do not depend on `x`, namely are constant. Default
                                is `False`.
    + `sample_generator`:   a function which returns a sample iterator on pairs of
                                vertices, by processing its two arguments, namely
                                the whole list of vertices and the variable
                                parameter `x`. Alternatively, this argument could
                                be an integer that specifies the number of pairs
                                to sample uniformly on the list of vertices of the
                                generated graphs.
    + `verbose`:            allows to display messages during the process, such
                                as the current value of the parameter `x`.
    """
    results = []
    for x in x_range:

        if verbose:
            print(" "*16+"\r{}".format(x), end='\r')
        
        # Preparing graph, sample, and reference shortest path algorithm
        if constant and results:
            # graph and sample are constant and already generated
            # ref_algo has already been performed.
            # we point it onto its results to prevent rerun the same computation a second time.
            ref_algo = results[0][-1]
        else:
            # Prepare graph
            G = graph_generator(x)
            Gnodes = list(G)
            # Prepare sample (sample_generator might be an integer, indicating the size of the sample to uniformly generate)
            if type(sample_generator) is int:
                S = [ random.sample(Gnodes, 2) for _ in range(sample_generator) ]
            else:
                S = sample_generator(Gnodes, x)
            # Preparing reference
            if reference.hfunc_gen is None:
                hfunc = None
            else:
                hfunc = reference.hfunc_gen(G, x)
            ref_algo = reference.sp_algo_gen(G, hfunc, x)

        # Preparing the sp_algo
        sp_algos = []
        for d in to_compare:
            if d.hfunc_gen is None:
                hfunc = None
            else:
                hfunc = d.hfunc_gen(G, x)
            sp_algos.append(d.sp_algo_gen(G, hfunc, x))

        res = sample_sp_comp(S, sp_algos, ref_algo)
        f_res = format_sp_results_comp(res, in_place=in_place)
        results.append(f_res)
    return results

def sample_sp_comp(sample, sp_algos, reference):
    """
    This function compares results of shortest path algorithms
    on a sample of pairs of vertices (source and target). The
    algorithm are given in the `sp_algos` list and should be
    functions (or callable objects) taking two arguments only,
    namely the source and the target vertices of the searched
    shortest path.
    Results are confronted to a reference algorithm passed as
    `reference` argument, or to static pre-computed results
    (which are gathered in a dictionary just like the elements
    of the list returned by the present function, as explained
    below).

    The computed results mainly include the size of the portion
    of the graph explored during the shortest path search. It
    also includes errors number and amplitude, by comparing the
    length of the found shortest path with the length of the
    shortest path found by the reference algorithm.

    The return value is a list of length length of `sp_algos`
    plus one, whose elements are dictionaries. The last element
    of the list corresponds to the results obtained through the
    reference algorithm (that possibly are statically given as
    argument of the function, to avoid recomputing many times
    the same reference algorithm). The results `dict` elements
    have the following keys:
    + 'distances'       # only the last reference `dict`
    + 'explored size'   # all `results` elements
    + 'error number'    # all but the last `results` elements
    + 'error amplitude' # all but the last `results` elements
    """

    compute_ref = not(type(reference) is dict)
    
    results = [ dict() for _ in sp_algos ]
    for res in results:
        #res['distances'] = []
        res['reference'] = False
        res['explored size'] = []
        res['error number'] = 0
        res['error amplitude'] = 0
        #res['fail number'] = 0
        #res['fail amplitude'] = 0

    # results last element corresponds to results associated with the reference algorithm
    if compute_ref:
        ref_res = dict()
        ref_res['reference'] = True
        ref_res['distances'] = []
        ref_res['explored size'] = []
        #results[-1]['source'] = []
        #results[-1]['target'] = []
    else:
        ref_res = reference
    results.append(ref_res)

    for sample_idx, (source, target) in enumerate(sample):

        if compute_ref:
            path, expl_size = reference(source, target)
            ref_dist = len(path)
            results[-1]['distances'].append(ref_dist)
            results[-1]['explored size'].append(expl_size)
            #results[-1]['source'].append(source)
            #results[-1]['target'].append(target)
        else:
            ref_dist = reference['distances'][sample_idx]

        for i, algo in enumerate(sp_algos):
            res = results[i]
            path, expl_size = algo(source, target)
            path_len = len(path)
            #res['distances'].append(path_dist)
            res['explored size'].append(expl_size)
            diff = path_len - ref_dist
            if diff > 0:
                res['error number'] += 1
                res['error amplitude'] += diff
            elif diff < 0:
                #a negative amplitude indicates that the reference
                #algorithm is not exact, or that the current algorithm
                #is wrong.  #Fail data are not returned
                print('/!\ Found path shorter than the reference one!')
                #res['fail number'] += 1
                #res['fail amplitude'] -= diff
                pass
    return results

def format_sp_results_comp(results, in_place=False):
    ref_distances = numpy.array(results[-1]['distances'])
    ref_explored = sum(results[-1]['explored size'])
    sample_size = len(results[-1]['distances'])
    if in_place:
        f_results = results
    else:
        f_results = []
    for res in results:
        if in_place:
            f_res = res
        else:
            f_res = dict()
            f_results.append(f_res)
        expl_size = numpy.array(res['explored size'])
        scores = expl_size/ref_distances
        total_expl_size = sum(expl_size)
        f_res['mean explored'] = mean(expl_size)
        f_res['stdev explored'] = stdev(expl_size)
        f_res['mean score explored'] = mean(scores)
        f_res['stdev score explored'] = stdev(scores)
        f_res['% explored'] = 100*total_expl_size/ref_explored
        if 'error number' in res:
            f_res['% error number'] = 100*res['error number']/sample_size
        if 'error amplitude' in res:
            f_res['% error amplitude'] = 100*res['error amplitude']/sample_size
    return f_results
