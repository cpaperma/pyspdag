import networkx as nx
import random
import math

###
def lm_sel_random(G, k=None, optimize_iterations=0):
    Gnodes = list(G)
    if k is None:
        k = int(math.log(len(Gnodes)+1))
    else:
        if k > len(Gnodes):
            raise ValueError("Cannot select {} landmarks in graph of smaller size {}".format(k, len(Gnodes)))
    lm_set = list(random.sample(Gnodes, k))
    #Though optimization is usually design for farthest selection method,
    #we allow (farthest) optimization for random selection as well (why not?).
    lm_optim_farthest(G, lm_set, optimize_iterations)
    return lm_set

###
def lm_sel_farthest(G, k=None, optimize_iterations=0):
    Gnodes = list(G)
    if k is None:
        k = int(math.log(len(Gnodes)+1))
    else:
        if k > len(Gnodes):
            raise ValueError("Cannot select {} landmarks in graph of smaller size {}".format(k, len(Gnodes)))
    v = random.choice(Gnodes)
    lm_set = [v]
    for i in range(k-1):
        lm_add_farthest(G, lm_set)
    lm_optim_farthest(G, lm_set, optimize_iterations)
    return lm_set

def lm_add_farthest(G, lm_set):
    d2lm_set = dict()
    for s in lm_set:
        d2lm = nx.shortest_path_length(G, s)
        for u in d2lm:
            if u in d2lm_set:
                d2lm_set[u] = min(d2lm[u], d2lm_set[u])
            else:
                d2lm_set[u] = d2lm[u]
    w = max(d2lm_set, key=d2lm_set.__getitem__)
    lm_set.append(w)

def lm_optim_farthest(G, lm_set, iteration):
    for _ in range(iteration):
        k = random.choice(lm_set)
        lm_set.remove(k)
        lm_add_farthest(G, lm_set)

###
def lm_idx(G, lm_set):
    Gnodes = list(G)
    lmidx = { lm: nx.shortest_path_length(G, lm) for lm in lm_set }
    return lmidx

###
def lm_hfunc(lmidx, underestimate=True):
    if underestimate:
        h = lambda u, v: max(map(lambda lm: abs(lmidx[lm][u]-lmidx[lm][v]), lmidx))
    else:
        h = lambda u, v: min(map(lambda lm: lmidx[lm][u]+lmidx[lm][v], lmidx))
    return h

###
def lm_create_hfunc(G, k=None, selection='random', optimize_iterations=0, underestimate=True):
    """
    This methods returns a heuristic function (mapping pairs of vertices to comparable values),
    based on a landmark embedding. The number of landmarks can be chosen through `k` parameter,
    why the way landmarks are chosen can be controlled by the `selection` parameter which may
    take value `'random'` or `'farthest'`. In both case, optimization of the landmark selection
    can be performed, by increasing the `optimize_iterations` parameter.
    Finally, the returned heuristic function can either underestimate or overestimate the node
    distance. The latter option is recommended since it yields an admissible heuristic function
    (default).
    """
    if selection == 'random':
        lm_set = lm_sel_random(G, k, optimize_iterations=optimize_iterations)
    elif selection == 'farthest':
        lm_set = lm_sel_farthest(G, k, optimize_iterations=optimize_iterations)
    else:
        raise ValueError("Unknown landmark selection mode {}. Please use one of ['random', 'farthest'].".format(selection))
    return lm_hfunc(lm_idx(G, lm_set), underestimate=underestimate)
